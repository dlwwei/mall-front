const path = require('path')

resolve = dir => path.resolve(__dirname, dir)
module.exports = {
  lintOnSave: false,
  devServer: {
    open: true,//设置自动打开
    port: 8080,//设置端口
    inline: true, // 实时刷新
    disableHostCheck: true,// 外网访问
    overlay: {
      warnings: true,
      errors: true
    },
    //proxy: 'http://localhost:8080',
    public: 'localhost:8080',  // 本地ip
/*    proxy: {  //配置跨域
      '/static': {//请求前缀
        target: 'http://127.0.0.1:80/',  //填写真实的后台接口
        changOrigin: true,//控制请求头中的host值，是否欺骗目标服务器
        pathRewrite: {
          /!* 重写路径，当我们在浏览器中看到请求的地址为：http://localhost:8080/api/core/getData/userInfo 时
            实际上访问的地址是：http://121.121.67.254:8185/core/getData/userInfo,因为重写了 /api
           *!/
          '^/static': ''//正则匹配，重写路径
        }
      },
    }*/
  },
  configureWebpack: {
    resolve: {
      //配置路径别名
      alias: {
        '@':resolve('src'),
        '@assets':resolve('src/assets'),
        '@components':resolve('src/components'),

      }
    }
  },
  pages: {
    index: {
      // page 的入口
      entry: 'src/main.js',
      // 模板来源
      template: 'public/index.html',
      // 在 dist/index.html 的输出
      filename: 'index.html',
      // 当使用 title 选项时，
      // template 中的 title 标签需要是 <title><%= htmlWebpackPlugin.options.title %></title>
      title: 'vue项目',
    },
  },

}
