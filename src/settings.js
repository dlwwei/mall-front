module.exports = {

  title: 'Vue Admin Template',

  // token存储名称
  tokenKey: 'satoken-front',

  // oss访问前缀
  prefix : 'https://mall-resource-bucket.oss-cn-hangzhou.aliyuncs.com/',

  staticPrefix : 'http://127.0.0.1:80',


  /**
   * @type {boolean} true | false
   * @description Whether fix the header
   */
  fixedHeader: false,

  /**
   * @type {boolean} true | false
   * @description Whether show the logo in sidebar
   */
  sidebarLogo: false
}
