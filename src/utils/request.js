import axios from 'axios'
import { MessageBox, Message } from 'element-ui'
import store from '@/store'
import { getToken } from '@/utils/auth-cookie'
import defaultSettings from '@/settings'

// 创建axios实例
const service = axios.create({
  /*baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url*/
  baseURL: "http://127.0.0.1:8088/", // url = base url + request url
  withCredentials: false, // 跨域请求时发送cookie
  timeout: 10000 // 请求超时时间
})

// request interceptor
service.interceptors.request.use(
  config => {
    console.log("请求拦截器-> ", config.method, config.url)

    // 在发送请求之前做些什么
    if (store.getters.token) {
      const tokenKey = defaultSettings.tokenKey || 'mall-front-token'
      // 让每个请求携带令牌['X-token']是一个自定义头密钥，请根据实际情况进行修改
      config.headers[tokenKey] = getToken()
    }
    return config
  },
  error => {
    // 处理请求错误
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// 响应拦截器
service.interceptors.response.use(
    /**
     * 如果您想获取http信息，如标题或状态，请返回response=>response
     */

        // 通过自定义代码确定请求状态这里只是一个示例，您也可以通过HTTP状态代码来判断状态
    response => {
      console.log("响应拦截器response-> ",response)
      const res = response.data

      // 如果响应码不是200，则判断为错误。 系统错误
      if (response.status !== 200) {
        Message({
          message: response.data.message || 'Error',
          type: 'error',
          duration: 5 * 1000
        })
        return Promise.reject(new Error(res.message || 'Error'))
      } else {
        // 人为错误
        if (res.code === 403 ) {
          // to re-login
          MessageBox.confirm(`${res.message}，可以取消以停留在此页面，或再次登录`, '确认注销', {
            cancelButtonText: '取消',
            confirmButtonText: '重新登陆',
            type: 'warning'
          }).then(() => {
            store.dispatch('user/resetToken').then(() => {
              location.reload()
            })
          })
        }else if(res.code === 666) {
          // to re-login
          MessageBox.confirm(`${res.message}`, '错误', {
            cancelButtonText: '取消',
            confirmButtonText: '刷新',
            type: 'warning'
          }).then(() => {
            location.reload()
          })
        }else {
          return res
        }
      }
    },
    error => {
      console.log('err' + error) // for debug
      Message({
        message: error.message + 111,
        type: 'error',
        duration: 5 * 1000
      })
      return Promise.reject(error)
    }
)

export default service
