import Cookies from 'js-cookie'

import defaultSettings from '@/settings'

const tokenKey = defaultSettings.tokenKey || 'mall-front-token'

export function getToken() {
  return Cookies.get(tokenKey)
}

export function setToken(token) {
  return Cookies.set(tokenKey, token)
}

export function removeToken() {
  return Cookies.remove(tokenKey)
}


export function getNickName() {
  return Cookies.get("nickname")
}

export function setNickName(value) {
  return Cookies.set("nickname", value)
}

export function removeNickName() {
  return Cookies.remove("nickname")
}
