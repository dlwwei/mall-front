import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
    mode: 'history', //去掉uri的#号，详情看官网
    scrollBehavior (to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition
        } else {
            return { x: 0, y: 0 }
        }
    },
    routes: [
        // 登录
        { path: '/login',name: 'login', component: ()=>import("@components/Login"), meta: {title: '登录',},},
        { path: '/register',name: 'register', component: ()=>import("@components/Register"), meta: {title: '注册',},},


        // 首页
        { path: '/main', redirect: "/"},
        { path: '/index', redirect: "/"},
        { path: '/' , component: ()=>import("@components/common/Main"), meta: {title: '主页面',},
            children: [
                //错误页面
                { path: '/notAuthorized', component: ()=>import("@components/common/error/403"),meta: {title: '没有权限',}},
                //首页
                { path: '/', component: ()=>import("@components/index/Index"),meta: {title: '电商系统首页',}},
                // 搜索页面
                { path: '/search/:keyWord', component: ()=>import("@components/index/Search"),meta: {title: '搜索',}},

                //个人中心
                { path: '/home', component: ()=>import("@components/home/Home"),meta: {title: '个人中心', auth: true}},
                // 个人地址
                { path: '/home/address', component: ()=>import("@components/home/Address"),meta: {title: '地址管理', auth: true}},
                // 购物车
                { path: '/shoppingCart', component: ()=>import("@components/shoppingCart/ShoppingCart"),meta: {title: '购物车',auth: true}},

                // 订单列表
                { path: '/order', component: ()=>import("@components/order/Order"),meta: {title: '订单',auth: true}},
                // 订单详情
                { path: '/orderDetail/:orderId', component: ()=>import("@components/order/OrderDetail"),meta: {title: '订单详情',auth: true}},

                // 支付成功
                { path: '/orderSuccess', component: ()=>import("@components/shoppingCart/OrderSuccess"),meta: {title: '支付成功',auth: true}},
                // 支付错误
                { path: '/orderError', component: ()=>import("@components/shoppingCart/OrderError"),meta: {title: '支付异常',}},

                // 类别分区
                { path: '/category/:category', component: ()=>import("@components/category/Category"),meta: {title: '分区',}},
                // 品牌
                { path: '/:category/brand/:brand', name:"brand", component: ()=>import("@components/category/Brand"), meta:{title: '品牌'},},

                //商品
                { path: '/product/:productId', component: ()=>import("@components/product/Product"),meta: {title: '产品',}},

            ]
        },

        /*      { path: '/index',name: 'index', component: Index, meta: {title: '首页',}},*/


        //错误页面
        { path: '/notAuthorized', component: ()=>import("@components/common/error/403"),meta: {title: '没有权限',}},

        //测试
        { path: '/test', component: ()=>import("@components/test/test"),meta: {title: '测试',}},

    ]

})

//push
const VueRouterPush = Router.prototype.push
Router.prototype.push = function push (to) {
    return VueRouterPush.call(this, to).catch(err => err)
}

//replace
const VueRouterReplace = Router.prototype.replace
Router.prototype.replace = function replace (to) {
    return VueRouterReplace.call(this, to).catch(err => err)
}
