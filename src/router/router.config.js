import router from "@/router/index";
import NProgress from "nprogress";//路由跳转进度条
import 'nprogress/nprogress.css'
import {getNickName, getToken} from '@/utils/auth-cookie' // get token from cookie

import store from '@/store'
import { Message } from 'element-ui'

const whiteList = ['/','/login','/register','/static/*'] // 无重定向白名单

router.beforeEach(async(to, from, next) => {

    NProgress.start()// 进度条开始

    if (to.matched.length === 0) {  // 如果未匹配到路由
        console.log("非法路径 =",to.path,"，将返回原页面 =",from.path)
        next('/'); // 如果上级也未匹配到路由则跳转登录页面，如果上级能匹配到则转上级路由
        return;
    }

    //console.log(getNickName())
    store.commit('user/SET_NAME', getNickName())

    // 确定用户是否已登录
    const hasToken = getToken()

    if (to.meta.auth) {
        console.log("需要权限")
        //console.log("router.beforeEach hasToken =",hasToken)

        // 如果有token 也就是已经登陆的情况下
        if (hasToken) {
            console.log("有token")
            console.log("router.beforeEach store.getters.roles =",store.getters.roles)

            // 确定用户是否已通过getInfo获得其权限角色
            // 从store中取得用户的 roles, 也就是用户的权限 并且用户的权限数组必须有一个以上
            const hasRoles = store.getters.roles && store.getters.roles.length > 0

            // 有权限则直接进入
            if (hasRoles) {
                next()
            } else {
                try {
                    //console.log("get user info")
                    // get user info
                    // 注意：角色必须是对象数组！例如：['admin']或，['developer'，'editor']
                    await store.dispatch('user/getInfo')

                    // hack方法确保addRoutes是完整的，设置replace:true，这样导航就不会留下历史记录
                    next({ ...to, replace: true })
                } catch (error) {
                    // 删除令牌并转到登录页面重新登录
                    await store.dispatch('user/resetToken')
                    Message.error(error || 'Has Error')
                    next(`/login`)
                    NProgress.done()
                }
            }
        } else {
            /* 没有token */
            if (whiteList.indexOf(to.path) !== -1) {
                console.log("没有token，但是在免费登录白名单中，直接进入")
                // 在免费登录白名单中，直接进入
                next()
            } else {
                console.log("没有token，并且其他没有访问权限的页面将重定向到登录页面")
                // 其他没有访问权限的页面将重定向到登录页面。
                next(`/login`)
                NProgress.done()
            }
        }
    }else {
        if (hasToken && to.path === '/login') {
            // 已经登录并且要前往的路径是'/login'  则返回 '/' （重定向到主页）
            next({ path: '/' })
            NProgress.done()
        }else {
            next();
        }
    }

})


/*
//前置路由守卫
router.beforeEach((to, from, next) => {
    NProgress.start()//开始进度条
    next()

/!*    if (to.matched.length === 0) {  //如果未匹配到路由
        console.log("非法路径 =",to.path,"，将返回原页面 =",from.path)
        console.log("from.matched =",from.matched)
        from.matched ? next(from.path) : next('/'); //如果上级也未匹配到路由则跳转登录页面，如果上级能匹配到则转上级路由
        return;
    }

    let user = JSON.parse(sessionStorage.getItem('user'));
    console.log("user =",user)

    if (to.meta.isLogin) {
        if (user === null){
            next('/');
        }
    }
    if (to.meta.isPermission){
        console.log("需要权限的页面 =",to.path)
        axios({
            method : "POST",
            url:"/info?method=permission",
        }).then((response)=>{
            console.log("response.data =", response.data, "to.path =", to.path)
            if (response.data === 0){
                next("/");
                return;
            }

            let role_name;
            switch (response.data.role_id) {
                case 1 : role_name = "super";break;
                case 2 : role_name = "admin";break;
                case 3 : role_name = "user";break;
            }
            user.role_name = role_name;
            //console.log("更新之后的user = ",user)
            sessionStorage.setItem("user",JSON.stringify(user));

            let isIncluded = response.data.permission.includes(to.path) || response.data.permission.includes("/!*");
            if (isIncluded){
                console.log("包含这个路径，可以访问")
                next();
            }else {
                next('/main/notAuthorized');
            }
        }).catch((error)=> {
            //this.$notify({title: '错误', message: '修改失败', type: 'error'});
        });
    }else {
        console.log("不需要权限的页面 =",to.path)
        next();
    }*!/

})
*/

//后置路由守卫
router.afterEach((to, from) => {
    document.title = to.meta.title;
    // 结束进度条
    NProgress.done()
})

export default router
