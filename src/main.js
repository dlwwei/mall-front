import Vue from 'vue'//引入vue
import App from './App.vue'//引入app组件
import store from './store'

import router from './router/router.config'//路由配置

//ElementUI
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

Vue.config.productionTip = false//关闭vue生产提示

new Vue({//创建vm
  router,//使用路由
  store,//使用vuex
  render: h => h(App),//渲染函数
}).$mount('#app')//挂载节点
