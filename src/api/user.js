import request from '@/utils/request'

// 用户登录
export function loginReq(data) {
  return request({
    url: '/user/login',
    method: 'post',
    data
  })
}

// 用户查重
export function getUserByUsernameReq(params) {
  return request({
    url: '/user/getUserByUsername',
    method: 'GET',
    params
  })
}

// 用户注册
export function registerReq(data) {
  return request({
    url: '/user/register',
    method: 'post',
    data
  })
}

// 每次路由变化获取信息
export function getInfo() {
  return request({
    url: '/user/info',
    method: 'get',
  })
}

// 注销
export function logout() {
  return request({
    url: '/user/logout',
    method: 'get'
  })
}
