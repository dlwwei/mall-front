import request from '@/utils/request'

// 获得商品详细信息
export function getProductDetail(productId) {
  return request({
    method: "GET",//
    url: `/product/${productId}`,
  })
}

// 获得商品规格
export function getProductSpecs(data) {
  return request({
    method: "POST",//
    url: `/product/specs`,
    data
  })
}

// 添加到购物车
export function addCart(data) {
  return request({
    method: "POST",//
    url: `/cart/add`,
    data,
  })
}


