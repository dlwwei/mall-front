import request from '@/utils/request'

// 搜索
export function search(params) {
  return request({
    method: "GET",//
    url: `/product/`,
    params,
  })
}

// 获取用户基本信息
export function getUserBasicInfo() {
  return request({
    method : "GET",
    url:"/user/basic",
  })
}

// 更新用户基本信息
export function updateUserBasicInfo(data) {
  return request({
    method: "POST",//修改个人信息
    url: "/user/modifyUserInfo",
    data
  })
}

// 获取首页产品信息
export function getIndexProductInfo() {
  return request({
    method : "GET",
    url:"/product/getIndexProductInfo",
  })
}


// 修改密码
export function modifyPasswordReq(data) {
  return request({
    url: '/user/modifyPassword',
    method: 'POST',
    data
  })
}


