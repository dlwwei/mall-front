import request from '@/utils/request'

// 根据 类别 获得所有品牌
export function getBrandByCategory(params) {
  return request({
    url: `/category/${params}`,
    method: 'GET',
  })
}

// 根据 类别 获得所有品牌
export function getBrandById(brandId, currentPage) {
  return request({
    url: `/brand/${brandId}/${currentPage}`,
    method: 'GET',
  })
}


