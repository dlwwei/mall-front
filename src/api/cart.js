import request from '@/utils/request'

// 获得购物车信息
export function getShoppingCart() {
  return request({
    method: "GET",//
    url: `/cart/all`,
  })
}

// 删除购物车
export function removeCart(data) {
  return request({
    method: "POST",//
    url: `/cart/remove`,
    data
  })
}

// 减少购物车某商品数量
export function decrease(data) {
  return request({
    method: "POST",//
    url: `/cart/decrease`,
    data,
  })
}

// 增加购物车某商品数量
export function increase(data) {
  return request({
    method: "POST",//
    url: `/cart/increase`,
    data,
  })
}



