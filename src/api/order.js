import request from '@/utils/request'


// 生成订单
export function orderGenerateRequest(data) {
  return request({
    method: "POST",//
    url: `/order/generate`,
    data
  })
}

// 查询订单详细信息
export function getOrderDetailInfoReq(params) {
  return request({
    method: "GET",//
    url: `/order/info`,
    params
  })
}

// 订单支付
export function getDefaultAddressReq() {
  return request({
    method: "GET",//
    url: `/address/default`,
  })
}

// 订单支付
export function payReq(params) {
  return request({
    method: "GET",//
    url: `/order/aliPay`,
    params
  })
}

// 查询历史订单
export function getHistoryOrderInfoReq(data) {
  return request({
    method: "POST",//
    url: `/order/history`,
    data
  })
}



