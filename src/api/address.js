import request from '@/utils/request'

// 获得所有的地址
export function getAllAddress() {
  return request({
    url: '/address/all',
    method: 'get',
  })
}

// 设置默认地址
export function setDefaultAddress(addressId) {
  return request({
    url: `/address/setDefault/${addressId}`,
    method: 'get',
  })
}

// 获得地址信息，用于修改
export function getAddress(addressId) {
  return request({
    url: `/address/${addressId}`,
    method: 'get'
  })
}

// 增加地址
export function addAddress(data) {
  return request({
    method: "POST",//
    url: "/address/add",
    data,
  })
}

// 更新地址
export function updateAddress(data) {
  return request({
    method: "POST",//
    url: "/address/update",
    data,
  })
}
