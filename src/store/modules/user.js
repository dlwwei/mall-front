import { loginReq, logout, getInfo } from '@/api/user'
import {getToken, setToken, removeToken, setNickName, removeNickName, getNickName} from '@/utils/auth-cookie'

import { Notification } from 'element-ui'

const getDefaultState = () => {
  return {
    token: getToken(),
    nickname: '' | getNickName(),
    avatar: '',
    roles: []
  }
}

const state = getDefaultState()

const mutations = {
  RESET_STATE: (state) => {
    Object.assign(state, getDefaultState())
  },
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_NAME: (state, nickname) => {
    state.nickname = nickname
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  },
  SET_ROLES: (state, roles) => {
    state.roles = roles
  }
}

import defaultSettings from '@/settings'
const tokenKey = defaultSettings.tokenKey || 'mall-front-token'


const actions = {

  login({ commit }, loginForm) {
    console.log(loginForm)
    return new Promise((resolve, reject) => {
      loginReq({loginForm}).then(response => {
        if (response.state === true){
          console.log("login =",response)
          commit('SET_TOKEN', response.data[tokenKey]) // vuex中保存token
          const { roles, nickname, avatar } = response.data.info
          // 角色必须是非空数组
          if (!roles || roles.length <= 0) reject('getInfo:角色必须是非空数组!')
          commit('SET_ROLES', roles)
          commit('SET_NAME', nickname)
          commit('SET_AVATAR', avatar)
          setNickName(nickname)
          console.log(getNickName())

          setToken(response.data[tokenKey]) // cookie中保存token
          //Notification.success({title: '成功', message: response.message});
          resolve()
        }else {
          Notification.warning({title: '失败', message: response.message});
          reject(error)
        }
      }).catch(error => {
        //Notification.error({title: '错误', message: '请求失败' + error});
        reject(error)
      })
    })
  },

  // get user info 每次路由变化都执行
  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      getInfo().then(response => {
        const { data } = response

        if (!data) {
          reject('验证失败，请重新登录')
        }

        const { roles, nickname, avatar } = response.data.info

        console.log("getInfo",roles, nickname, avatar)
        // 角色必须是非空数组
        if (!roles || roles.length <= 0) {
          reject('getInfo:角色必须是非空数组!')
        }

        commit('SET_ROLES', roles)
        commit('SET_NAME', nickname)
        commit('SET_AVATAR', avatar)
        resolve(response.data.info)
      }).catch(error => {
        reject(error)
      })
    })
  },

  // user logout
  logout({ commit, state }) {
    return new Promise((resolve, reject) => {
      logout().then(() => {
        removeToken() // 删除cookie中的token
        removeNickName()// 删除cookie中的昵称
        commit('RESET_STATE')
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      removeToken() // 必须先删除令牌
      commit('RESET_STATE')
      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

