# mall-front

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

path.dirname()  :获取目录
 
 2、path.basename() ：获取文件名.扩展名(我们统称为全名)
 
 3、path.extname()  : 获取扩展名
 
 4、path.parse()    : 将一个路径转换成一个js对象
 
 5、path.format()   ：将一个js对象转换成路径
 
 6、join()          : 拼接多个路径成一个路径
 
 7、path.resolve() :将相对路径转为绝对路径
 __dirname: 总是返回被执行的 js 所在文件夹的绝对路径
 __filename: 总是返回被执行的 js 的绝对路径